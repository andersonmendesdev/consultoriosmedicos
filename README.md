# Finalizado att Anderson

Instalar dependencias
```bash
yarn install
```
Inicilizar app
```bash
yarn start
```
Start manual
```bash
banco mongo
```

# consultórios médicos

Deverá ser desenvolvido em NodeJS e o banco de dados fica a critério do desenvolvedor.  
Commitar sempre que achar necessário.   
**Deverá ser entregue até as 09 horas do dia 11/julho/2018. (quarta)**  
*(quanto mais cedo a entrega, melhor)*

#### introdução
O sistema deve ter uma interface web, sem necessidade de login. CRUD simples para as entidades criadas. Cadastro de médicos, especialidades e consultórios.  

#### entidades
Especialidades: nome da especialidade.  
Médico: nome do médico.  
Consultório: nome do consultório.  

#### observações
- O consultório pode ter várias especialidades.  
- O médico pode ter várias especialidades.  
- Somente poderá cadastrar médicos ao consultório se o consultório tiver a especialidade que o médico tem.  
- Um médico pode atender em vários consultórios.  
- Um consultório pode ter vários médicos.  
