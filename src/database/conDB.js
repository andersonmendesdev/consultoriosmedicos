const mongoose = require('mongoose')
const mongo = process.env.MONGOURL || 'mongodb://localhost:27017/consultoriomedico'

try{
    mongoose.connect(mongo, { useNewUrlParser: true })
    mongoose.Promise = global.Promise

}catch(e){
    console.log(e)
}

module.exports = mongoose