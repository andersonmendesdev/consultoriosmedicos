const indexMedico = async( { Medico }, req, res) =>{
    try{
        const medico = await Medico.find({})    
        res.render('medico/index', { medico})

    }catch(e){
        console.log(e)
    }

}

const createForm = async({ Especialidade },req, res) => {
    try{
        const especialidade = await Especialidade.find({})
        res.render('medico/create', { especialidade, error: false, errorEmpty: false })

    }catch(e){
        console.log(e)
    }

}

const createModelMedico = async({ Medico, Especialidade }, req, res) =>{
    try{
        const especialidade = await Especialidade.find({})
        const Isvalid = await Medico.findOne({ crm: req.body.crm})
        if(!req.body.especialidade){
           return res.render('medico/create', { especialidade, error: false, errorEmpty: true, Isvalid }) 
        }
        if(!req.body.name){
           return res.render('medico/create', { especialidade, error: false, errorEmpty: true, Isvalid }) 
        }
        if(!req.body.crm){
            return res.render('medico/create', { especialidade, error: false, errorEmpty: true, Isvalid })
        }
        if(Isvalid){
           return res.render('medico/create', { especialidade, error: true, errorEmpty: false, Isvalid })
        }       
            
        await Medico.create(req.body)
        res.redirect('/medico')
                    
    }catch(e){
        console.log(e)
    }
        
}

const editForm = async({ Medico , Especialidade },req, res) =>{
    try{
        const medico = await Medico.findOne({ _id:req.params.id })
        const especialidade = await Especialidade.find({})
        res.render('medico/edit', { medico , especialidade })

    }catch(e){
        console.log(e)
    }

}

const deleteMedico = async({Medico}, req, res) => {
    try{
        await Medico.remove({ _id: req.params.id })
        res.redirect('/medico')

    }catch(e){
        console.log(e)
    }

}

module.exports = {
    indexMedico, createForm, createModelMedico, editForm, deleteMedico
}