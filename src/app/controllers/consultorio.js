function status(arr1, arr2){
    let val = false
    arr1.forEach((e1)=>arr2.forEach((e2)=>{
        if(e1 === e2){
            val = true
        }
    }))
    return val
}
function stats(id, arr1){
    let val = false
    arr1.map((medid) => {
        if(medid.id == id){
            val = true
         }
    })
    return val
}

const home = async({ Consultorio }, req, res) => {
    try{
        const consultorio = await Consultorio.find({})
        res.render('home', { consultorio })

    }catch(e){
        console.log(e)
    }

}

const indexConsultorio = async( { Consultorio }, req, res) => {
    try{
        const consultorio = await Consultorio.find().populate('medico', 'name')
        res.render('consultorio/index', {consultorio})

    }catch(e){
        res.redirect('/consultorio')
    }

}

const createForm = async({ Especialidade },req, res) => {

    try{
        const especialidade = await Especialidade.find({})
        res.render('consultorio/create', { especialidade , errorEmpty: false, error: false })

    }catch(e){
        res.redirect('/consultorio')
    }

}

const createConsultorio = async({Consultorio, Especialidade}, req, res) =>{
    try{
        const especialidade = await Especialidade.find({})
        const Isvalid = await Consultorio.findOne({ nameConsultorio: req.body.nameConsultorio})
        if(!req.body.especialidade || !req.body.nameConsultorio){
           return res.render('consultorio/create', { especialidade , errorEmpty: true, error: false, Isvalid })
        }          
        if(Isvalid){
            return  res.render('consultorio/create', { especialidade , errorEmpty: false, error: true, Isvalid })
        }
        await Consultorio.create(req.body)
        res.redirect('/consultorio')

    }catch(e){
        res.redirect('/consultorio')
    }     
}
const infoConsultorio = async({Consultorio}, req, res) =>{
    try{
        const consultorio = await Consultorio.findOne({_id: req.params.id}).populate('medico', 'name')
        res.render('consultorio/info', { consultorio })

    }catch(e){
        res.redirect('/')
    }
}

const addConsultorio = async({Consultorio, Medico},req, res) => {
    try{
        const medico = await Medico.find({}) 
        const consultorio = await Consultorio.findOne({_id: req.params.id}).populate('medico')
        res.render('consultorio/medico',{consultorio, medico, errorEspec: false, errorMedico: false})

    }catch(e){
        
        res.redirect('/consultorio')
    }
}

const Addmedicos = async({Consultorio, Medico},req, res) => {

    try{
        const medico = await Medico.find({})
        const oneMedico = await Medico.findOne({_id: req.query.idmedico})
        const consultorio = await Consultorio.findOne({_id: req.query.idconsultorio}).populate('medico')

        if(stats(req.query.idmedico, consultorio.medico)){            
            return res.render('consultorio/medico',{consultorio, medico, errorEspec: false, errorMedico: true})    
        }       
        if(!status(consultorio.especialidade,oneMedico.especialidade)){
            return res.render('consultorio/medico',{consultorio, medico, errorEspec: true, errorMedico: false})
        }
        await consultorio.medico.push(req.query.idmedico)
        await consultorio.save()
        res.redirect('/consultorio')            
        
    }catch(e){
        res.redirect('/consultorio')
    }
       
}

const removeConsultorio = async({ Consultorio }, req, res) =>{
    try{
        await Consultorio.remove({ _id: req.params.id })
        res.redirect('/consultorio')

    }catch(e){
        res.redirect('/consultorio')
    }

}

const removeMedico = async ({Consultorio, Medico}, req, res) => {  
    try{
        
        const consultorio = await Consultorio.findOne({_id: req.query.idcons}).populate('medico')
        await consultorio.medico.splice(consultorio.medico.indexOf(req.query.idrm),1)
        await consultorio.save()        
        res.redirect('/consultorio')

    }catch(e){
        res.redirect('/consultorio')
    } 
}

module.exports = {
    home, indexConsultorio, createForm, createConsultorio, infoConsultorio, addConsultorio, Addmedicos, removeConsultorio, removeMedico
}