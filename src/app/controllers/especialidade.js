const indexEspecilidade = async({ Especialidade }, req, res) =>{
    try{
        const especialidade = await Especialidade.find({})
        res.render('especialidade/index', {especialidade})

    }catch(e){
        console.log(e)
    }

}
    
const createForm = async({ Especialidade },req, res) => {
    try{
        res.render('especialidade/create', {error: false, errorempty: false})

    }catch(e){
        console.log(e)
    }
    
}

const createModelEspecialidade = async({ Especialidade }, req, res) =>{
    try{
        const Isvalid = await Especialidade.findOne({name: req.body.name})
        if(!req.body.name){
            return res.render('especialidade/create',{errorempty: true, error: false})
        }
        if(Isvalid){        
            return res.render('especialidade/create',{errorempty: false, error: true, Isvalid })
        }
      
        await Especialidade.create(req.body)
        res.redirect('/especialidade') 

    }catch(e){
        console.log(e)
    }

}

const deleteEspecialidade = async({ Especialidade }, req, res) =>{
    try{
        await Especialidade.remove({ _id: req.params.id })
        res.redirect('/especialidade')

    }catch(e){
        console.log(e)
    }

}

module.exports = {
    indexEspecilidade, createForm , createModelEspecialidade, deleteEspecialidade
}