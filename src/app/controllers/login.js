const ControllersCreate = require('./CreateiniUser')

const index = async(req, res) => {
    ControllersCreate.CreateInitialUser()
    if(req.user){
       return res.redirect('/')
    }
    res.render('login/index', {error: false} )
}

const logoutUser = async(req, res) => {
    req.session.destroy(() => {
        res.redirect('/login')
    })

}

module.exports = {
    index, logoutUser
}