const mongoose = require('../../database/conDB')

const especialidadeSchema = new mongoose.Schema({
    name: {
        type: String,
        unique: true,
        lowercase: true
    }
})

const Especialidade = mongoose.model('Especialidade', especialidadeSchema)
 module.exports = Especialidade