const mongoose = require('../../database/conDB')

const medicoSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    crm: {
        type: String,
        unique: true
    },
    especialidade:{
        type: [String],
        lowercase: true
    },
    createdAt: {
        type: Date,
        default: Date.now
    }
})


const Medico = mongoose.model('Medico', medicoSchema)
module.exports = Medico