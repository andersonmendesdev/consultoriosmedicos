const mongoose = require('../../database/conDB')

const consultorioSchema = new mongoose.Schema({
    nameConsultorio: {
        type: String
    },
    especialidade:{
        type: [String],
        lowercase: true
    },
    medico:[{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Medico'
    }],
    createdAt: {
        type: Date,
        default: Date.now
    }
})

const Consultorio = mongoose.model('Consultorio', consultorioSchema)

module.exports = Consultorio