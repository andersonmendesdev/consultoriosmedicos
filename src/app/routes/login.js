const express = require('express')
const router = express.Router()
const User = require('../models/user')
const ControllersLogin = require('../controllers/login')
const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy
const bcrypt = require('bcrypt')


router.use(passport.initialize())
router.use(passport.session())

passport.serializeUser((user, done) => {
    done(null, user)
})
passport.deserializeUser((user, done) => {
    done(null, user)
})

passport.use(new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password',
    session: false
  },
  async (email , password, done) => {
    const user = await User.findOne({ email: email}).select('+password')
    if(!user){
        return done(null, false)
    }
    if(!await bcrypt.compare(password, user.password)){
        return done(null, false)
    }
    return done(null, user)
  }
))

router.get('/logout', ControllersLogin.logoutUser)
router.get('/login', ControllersLogin.index)
router.post('/login', passport.authenticate('local', {    
    successRedirect: '/',
    failureRedirect: '/login',
    failureFlash: false

}))

module.exports = router