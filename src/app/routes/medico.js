const express = require('express')
const router =  express.Router()
const Medico = require('../models/medico')
const Especialidade = require('../models/especialidade')
const ControllersMedico = require('../controllers/medico')

const models = {
    Medico, Especialidade
}
router.use((req, res, next) => {
    if(req.isAuthenticated()){
        return next()
    }
    res.redirect('/login')
})

router.use((req, res, next) => {
    if(req.isAuthenticated()){
        res.locals.user = req.user
    }
    next()
})
router.get('/',ControllersMedico.indexMedico.bind(null, models))
router.post('/create',ControllersMedico.createModelMedico.bind(null, models))
router.get('/create', ControllersMedico.createForm.bind(null, models))
router.get('/edit/:id',ControllersMedico.editForm.bind(null, models))
router.get('/delete/:id', ControllersMedico.deleteMedico.bind(null, models))


module.exports = router