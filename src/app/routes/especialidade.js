const express = require('express')
const router =  express.Router()
const Especialidade = require('../models/especialidade')
const ControllersEspecialidade = require('../controllers/especialidade')

const models = {
    Especialidade
}

router.use((req, res, next) => {
    if(req.isAuthenticated()){
        return next()
    }
    res.redirect('/login')
})

router.use((req, res, next) => {
    if(req.isAuthenticated()){
        res.locals.user = req.user
    }
    next()
})
router.get('/',ControllersEspecialidade.indexEspecilidade.bind(null, models ))
router.post('/create', ControllersEspecialidade.createModelEspecialidade.bind(null, models))
router.get('/create', ControllersEspecialidade.createForm.bind(null, models ))
router.get('/delete/:id', ControllersEspecialidade.deleteEspecialidade.bind(null, models))


module.exports = router