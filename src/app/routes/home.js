const express = require('express')
const router = express.Router()
const Consultorio = require('../models/consultorio')
const ControllersConsultorio = require('../controllers/consultorio')
const models ={
    Consultorio
}

router.use((req, res, next) => {
    if(req.isAuthenticated()){
        return next()
    }
    res.redirect('/login')
})

router.use((req, res, next) => {
    if(req.isAuthenticated()){
        res.locals.user = req.user
    }
    next()
})
router.get('/', ControllersConsultorio.home.bind(null, models))

module.exports = router