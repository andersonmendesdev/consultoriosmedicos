const express = require('express')
const router =  express.Router()
const Consultorio = require('../models/consultorio')
const Especialidade = require('../models/especialidade')
const Medico = require('../models/medico')
const ControllersConsultorio = require('../controllers/consultorio')

const models = {
    Consultorio, Especialidade , Medico
}

router.use((req, res, next) => {
    if(req.isAuthenticated()){
        return next()
    }
    res.redirect('/login')
})

router.use((req, res, next) => {
    if(req.isAuthenticated()){
        res.locals.user = req.user
    }
    next()
})
router.get('/',ControllersConsultorio.indexConsultorio.bind(null, models ))
router.get('/create', ControllersConsultorio.createForm.bind(null, models))
router.post('/create', ControllersConsultorio.createConsultorio.bind(null, models))
router.get('/info/:id', ControllersConsultorio.infoConsultorio.bind(null, models))
router.get('/medico/:id', ControllersConsultorio.addConsultorio.bind(null, models))
router.get('/removemedico',ControllersConsultorio.removeMedico.bind(null, models))
router.get('/insert',ControllersConsultorio.Addmedicos.bind(null, models))
router.get('/delete/:id',ControllersConsultorio.removeConsultorio.bind(null, models))


module.exports = router