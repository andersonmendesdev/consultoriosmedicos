const express = require('express')
const path = require('path')
const app = express()
const port = process.env.PORT || 8081
const bodyParser = require('body-parser')
const session = require('express-session')
const http = require('http').Server(app)
const routerCons = require('./src/app/routes/consultorio')
const routerMed = require('./src/app/routes/medico')
const routerEsp = require('./src/app/routes/especialidade')
const routerHome = require('./src/app/routes/home')
const routerLogin = require('./src/app/routes/login')


//body
app.use(bodyParser.urlencoded({ extended: true}))

//folder public
app.use(express.static('./src/public'))

//view
app.set('views', path.join(__dirname, '/src/app/views'))
app.set('view engine', 'ejs')

//
app.use(session({ 
    secret: 'andersonmendesdev',
    resave: false,
    saveUninitialized: true 
}))
//router

app.use('/', routerLogin)
app.use('/', routerHome)
app.use('/consultorio',routerCons)
app.use('/medico', routerMed)
app.use('/especialidade', routerEsp)


http.listen(port, ()=> console.log('Server listening in '+port))